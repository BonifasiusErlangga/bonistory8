from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from .forms import SignUpForm

def home(request):
    return render(request, 'main/home.html')

def fungsi_suatu_url(request):
    response={}
    return render(request,'main/html_suatu_url.html', response)

def signupView(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            #print(User.objects.count())
            form.save()
            #print(User.objects.count())
            return redirect('/suatu_url')
    else:
        form = SignUpForm()
    response = {
        "form":form,
    }
    return render(request,"main/signup.html", response)


def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    kembalian = requests.get(url)
    data = json.loads(kembalian.content)
    
    if not request.user.is_authenticated:
        i = 0
        for x in data['items']:
            del data['items'][i]['volumeInfo']['imageLinks']['smallThumbnail']
            i+=1

    return JsonResponse(data, safe=False)
