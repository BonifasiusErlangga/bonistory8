from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from . import views
import requests, json

class SearchBook(TestCase):
    def test_adakah_url_formulir(self):
        response = self.client.get('/suatu_url/')
        self.assertEquals(response.status_code, 200)

    def test_adakah_templatenya(self):
        response = Client().get('/suatu_url/')
        self.assertTemplateUsed(response, 'main/html_suatu_url.html')

    def test_adakah_url_formulir_ada_text_BUKU_PESERTA(self):
        response = Client().get('/suatu_url/')
        html_return = response.content.decode('utf8')
        self.assertIn("Masukkan Keyword", html_return)

    def test_function(self):
        cari = resolve('/suatu_url/')
        self.assertEqual(cari.func, views.fungsi_suatu_url)
